document.addEventListener("DOMContentLoaded", function() {
    const table = document.querySelector('.students-table');

    const addButton = document.querySelector('#add-students-row-button');
    
    if(addButton) {
        addButton.addEventListener('click', function() {
            const newRow = document.createElement('tr');
            newRow.innerHTML = '<td><input type="checkbox" class="table-input"></td>' +
            '<td>PZ-21</td>' +
            '<td>Stas sdfkhghsjkdh</td>' +
            '<td>M</td>' +
            '<td>22.12.2005</td>' +
            '<td>' +
                '<i class="bi bi-circle-fill status"></i>' +
            '</td>' +
            '<td>' +
                '<div class="d-flex justify-content-center align-items-center mx-auto">'+
                    '<button class="btn-icon me-2">' +
                    '<i class="bi bi-pencil table-icons"></i>' +
                '</button>' +
                '<button class="btn-icon delete-students-row-button">' +
                    '<i class="bi bi-x-lg table-icons"></i>' +
                '</button>' +
                '</div>'+
            '</td>';
            table.getElementsByTagName("tbody")[0].appendChild(newRow);
        });
    }

    if (table) {
        table.addEventListener('click', function(event) {
            if (event.target.closest('.delete-students-row-button')) {
                const button = event.target.closest('.delete-students-row-button');
                const row = button.closest('tr');
                row.remove();
            }
        });
    }
});